﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

	public void SceneLoad(string p_scene) {
		UnityEngine.SceneManagement.SceneManager.LoadScene(p_scene);
	}
	
}
