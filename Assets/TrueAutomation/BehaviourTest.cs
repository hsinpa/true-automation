﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.TestTools;
using NUnit.Framework;
using System;
using System.IO;
using UnityEngine.SceneManagement;
using TrueAutomation;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BehaviourTest
{
    protected string sceneName;
    protected int behaviourCount;
    protected string[] behaviours;
    protected float[] behaviourTimes;
    protected Vector3[] positions;
    protected string[] targetNames;
    protected string[] targetPaths;
    
    EditorMouse editorMouse;
    int step;

    [SetUp]
    public void Setup()
    {

    }

	[UnityTest]
	public IEnumerator BehaviourTestWithEnumeratorPasses()
    {
        StreamReader reader = new StreamReader("Assets/StreamingAssets/recording_files/recordJSONFile.json");
        LoadTestBehaviour(reader.ReadToEnd());
        reader.Close();
        SceneManager.LoadScene(sceneName);
        yield return null;

        
        editorMouse = new EditorMouse(EventSystem.current);
        
        float time = 0;
        for (step = 0; step < behaviourCount; step++)
        {
            yield return new WaitForSeconds(behaviourTimes[step] - time);
            time = behaviourTimes[step];
            
            switch (behaviours[step])
            {
                case "MouseDown":
                    MouseDown();
                    break;
                case "Hover":

                    break;
            }
        }

        if (GameObject.Find("Input") != null)
            GameObject.Find("Input").GetComponent<RecorderManager>().recordModeActivate = false;
    }

    public virtual void LoadTestBehaviour(string jsonStream)
    {
        try
        {
            JSONObject jsObject = new JSONObject(jsonStream);
            List<JSONObject> jsObjectList = jsObject.list;
            behaviourCount = jsObject.list.Count - 1;
            behaviours = new string[behaviourCount];
            behaviourTimes = new float[behaviourCount];
            positions = new Vector3[behaviourCount];
            targetNames = new string[behaviourCount];
            targetPaths = new string[behaviourCount];
            sceneName = jsObjectList[0].GetField("name").str;
            for (int i = 1; i < behaviourCount; i++)
            {
                behaviours[i] = jsObjectList[i].GetField("type").str;
                behaviourTimes[i] = jsObjectList[i].GetField("time").f;
                positions[i] = new Vector3(jsObject[i].GetField("mousePosition").list[0].f, jsObject[i].GetField("mousePosition").list[1].f, 0f);
                targetNames[i] = jsObject[i].GetField("name").str;
                targetPaths[i] = jsObject[i].GetField("full_path").str;
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    protected virtual void Hover()
    {
        if (editorMouse.GetHitObject(positions[step]).gameObject != null)
        {
            GameObject obj = editorMouse.GetHitObject(positions[step]).gameObject;
            Debug.Log(obj.name);
            if (obj.name == "Label")
            {
                obj.transform.GetComponentInParent<Button>().OnPointerEnter(new PointerEventData(EventSystem.current));
                Debug.Log("hover");
            }
        }
    }

    protected virtual void MouseDown()
    {
        if (editorMouse.GetHitObject(positions[step]).gameObject != null)
        {
            GameObject obj = editorMouse.GetHitObject(positions[step]).gameObject;
            if (obj.transform.GetComponentInParent<Button>() != null)
            {
                obj.transform.GetComponentInParent<Button>().onClick.Invoke();
            }
            else if (obj.transform.GetComponentInParent<Toggle>() != null)
            {
                obj.transform.GetComponentInParent<Toggle>().isOn = !obj.transform.GetComponentInParent<Toggle>().isOn;
            }
        }
    }

}
#endif