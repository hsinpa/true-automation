﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using TrueAutomation;

public class SimpleTestCase {

    InputManager inputManager;
    EditorMouse editorMouse;
    List<JSONObject> jsonList;

    [SetUp]
    public void SetupTest() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu 3D");

    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator SimpleteInitialize() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return new WaitForSeconds(1);
        GameObject inputObject =  GameObject.Find("Input");
        Debug.Log(inputObject.name);   
        if (inputObject != null) {
            inputManager = inputObject.GetComponent<InputManager>();
            RecorderManager recordManager = inputObject.GetComponent<RecorderManager>();
            recordManager.recordModeActivate = false;

            inputManager.isDebuging = true;

            editorMouse = inputManager.mouseInput as EditorMouse;
            

            string rawJSONText = FileSystem.GetFileWithPath(Application.streamingAssetsPath+"/recording_files/recordJSONFileTest.json");
            jsonList = new JSONObject(rawJSONText).list;
        }
    }

    [UnityTest]
    public IEnumerator TestMouseClick() {
        yield return new WaitForEndOfFrame();

        for (int i = 0; i < jsonList.Count; i++) {
           if (jsonList[i].GetField("type").str == "MouseDown") {
            
               
               break;
           }

        }



    }

}
