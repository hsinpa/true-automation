﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

public class FileSystem  {
	
	public JSONObject ConvertInputToJSON(List<InputInfoComponent> inputList, JSONObject p_preloadJSON = null) {	
		JSONObject jObjectHolder = (p_preloadJSON == null) ? new JSONObject("[]") : p_preloadJSON;

		for(int i = 0; i < inputList.Count; i++) {
			JSONObject jObject = new JSONObject(JSONObject.Type.OBJECT);
			JSONObject jMousePosition = new JSONObject("[]");
			JSONObject jWorldPosition = new JSONObject("[]");

			jMousePosition.Add(inputList[i].mousePostion.x);
			jMousePosition.Add(inputList[i].mousePostion.y);

			jWorldPosition.Add(inputList[i].worldPosition.x);
			jWorldPosition.Add(inputList[i].worldPosition.y);
			jWorldPosition.Add(inputList[i].worldPosition.z);


			jObject.SetField("name", inputList[i].name);
			jObject.SetField("full_path", inputList[i].fullPath);
			
			jObject.SetField("type", inputList[i].type);
			jObject.SetField("mousePosition", jMousePosition);
			jObject.SetField("worldPosition", jWorldPosition);

			jObject.SetField("time", inputList[i].time);

			jObjectHolder.Add(jObject);
		}

		return jObjectHolder;
	}

	public static string GetFileWithPath(string p_full_path) {
		string readText = File.ReadAllText(p_full_path);
		return readText;
	}

	public void SaveFile(string file_name, string p_full_text, string relative_path) {
		string path = Application.streamingAssetsPath+ "/" +relative_path;
		string fullPath = path+"/"+file_name + ".json";

		if (!Directory.Exists(path)) {
			Directory.CreateDirectory(path);
		}

		try
        {

            // Delete the file if it exists.
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

            // Create the file.
            using (FileStream fs = File.Create(fullPath))
            {
                System.Byte[] info = new UTF8Encoding(true).GetBytes(p_full_text);
                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }

		}
		catch (System.Exception ex)
        {
        }

	}
	
}
