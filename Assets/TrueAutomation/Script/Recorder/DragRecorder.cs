﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace  TrueAutomation
{
	public class DragRecorder {
		InputManager inputManager;
		
		float defineDragRange = 20;
		public bool onDragStart;
		public GameObject lastHitObject;
		Vector3 lastMouseDownPos;



		public DragRecorder(InputManager p_inputManager) {
			inputManager = p_inputManager;
		}
		
		// Update is called once per frame
		public void Execute () {

			if (inputManager.mouseInput.OnMouseDown(0)) {
				RaycastResult hitObject = inputManager.mouseInput.GetHitObject( Input.mousePosition );

				lastMouseDownPos = Input.mousePosition;
				lastHitObject = hitObject.gameObject;
			}




			if (inputManager.mouseInput.OnMouse(0)) {

				if ((lastMouseDownPos - Input.mousePosition).magnitude > defineDragRange || onDragStart ) {
					onDragStart = true;

				}
			}

			if (inputManager.mouseInput.OnMouseUp(0)) {
				onDragStart = false;
				lastHitObject = null;
			}


		}
	}
	
}
