﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueAutomation;
using UnityEngine.EventSystems;

namespace TrueAutomation {
[RequireComponent(typeof(InputManager))]
	public class RecorderManager : MonoBehaviour {
		InputManager inputManager;
		List<InputInfoComponent> inputInfoList = new List<InputInfoComponent>();
		InputInfoComponent lastHitComponent;

		FileSystem fileSystem;
		DragRecorder dragRecorder;

		public bool recordModeActivate;
		private string startSceneName;

		// Use this for initialization
		void Start () {
			if(inputManager == null && fileSystem == null) {
				 DontDestroyOnLoad(this.gameObject);
				inputManager = GetComponent<InputManager>();
				fileSystem = new FileSystem();
				startSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
				dragRecorder = new DragRecorder(inputManager);
			} 
		}

		void OnGUI()
		{
			if (recordModeActivate) {
				GUI.Label(new Rect(10, 10, 300, 20), "Record Mode Activate!!");
			}
		}

		
		InputInfoComponent GetInputComponent(GameObject p_gameObject, string p_event, Vector2 p_mousePosition,
			Vector3 p_worldPosition, float p_time) {
			InputInfoComponent inputComponent = new InputInfoComponent();
			inputComponent.name = p_gameObject.name;
			inputComponent.type = p_event;
			inputComponent.fullPath = Utilities.GetGameObjectPath(p_gameObject);

			inputComponent.mousePostion = p_mousePosition;
			inputComponent.worldPosition = p_worldPosition;

			inputComponent.time = p_time;

			return inputComponent;
		}


		// Update is called once per frame
		void Update () {

			if (recordModeActivate) {
				RecordHover();
				RecordButtonClick();

				//Drag
				dragRecorder.Execute();
				if (dragRecorder.onDragStart) {
					InputInfoComponent comp =  GetInputComponent(dragRecorder.lastHitObject.gameObject, "Dragging", 
					Input.mousePosition, Input.mousePosition, Time.time);
					inputInfoList.Add(comp);
				}
			}

		}

		void OnApplicationQuit() {
			if (recordModeActivate) {
				JSONObject preloadJson = new JSONObject("[]");

				JSONObject sceneJSON = new JSONObject();
				sceneJSON.SetField("name",  startSceneName);
				preloadJson.Add(sceneJSON);

				JSONObject rawJSON = fileSystem.ConvertInputToJSON(inputInfoList, preloadJson);
				fileSystem.SaveFile( "recordJSONFile", rawJSON.ToString(), "recording_files");
			}
		}

		private void RecordHover() {
			List<UnityEngine.EventSystems.RaycastResult>  raycastList = inputManager.mouseInput.Hover(
				(Input.mousePosition) );

			//Hover
			if (raycastList.Count > 0) {
				RaycastResult rayResult = raycastList[0];

				InputInfoComponent comp =  GetInputComponent(rayResult.gameObject, "Hover", 
					rayResult.screenPosition, rayResult.worldPosition, Time.time);

				if (lastHitComponent.name != comp.name && lastHitComponent.type != comp.type) {
					inputInfoList.Add(comp);
					lastHitComponent = comp;
				}
			}
		}

		private void RecordButtonClick() {

			if (inputManager.mouseInput.OnMouseDown(0)) {
				RaycastResult hitObject = inputManager.mouseInput.GetHitObject( Input.mousePosition );
				if ( hitObject.isValid) {

					InputInfoComponent comp =  GetInputComponent(hitObject.gameObject, "MouseDown", 
					hitObject.screenPosition, hitObject.worldPosition, Time.time);
					inputInfoList.Add(comp);

					lastHitComponent = comp;
				}
			}
		}

		private void RecordDragging() {
			
		}







	}
}

