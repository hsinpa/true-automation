﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct InputInfoComponent {

	public string name;
	public string fullPath;
	public string type;
	
	public Vector2 mousePostion;
	public Vector3 worldPosition;

	public float time;
}
