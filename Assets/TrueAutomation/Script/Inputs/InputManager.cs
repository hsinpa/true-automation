﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace  TrueAutomation
{	
	public class InputManager : MonoBehaviour {
		Camera _mainCamera;
		EventSystem eventSystem;

		public BaseMouse mouseInput;
		public bool isDebuging;


		// Use this for initialization
		void Start () {
			_mainCamera  = Camera.main;
			eventSystem= EventSystem.current;

			if (!isDebuging) {
				mouseInput = new TrueInputMouse(eventSystem);
			} else {
				mouseInput = new EditorMouse(eventSystem);	
			}	

		}
		
	}
		
}
