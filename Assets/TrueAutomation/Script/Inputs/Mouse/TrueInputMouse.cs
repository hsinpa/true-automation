﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TrueAutomation {

public class TrueInputMouse : BaseMouse {

		public TrueInputMouse(EventSystem eventSystem) : base(eventSystem){
			
		}	

		public override bool OnMouseDown(int p_event_id) {
			return Input.GetMouseButtonDown(p_event_id);
		}
		public override bool OnMouseUp(int p_event_id) {
			return Input.GetMouseButtonUp(p_event_id);

		}
		public override bool OnMouse(int p_event_id) {
			return Input.GetMouseButton(p_event_id);
		}

		public override void Tracking(Vector3 p_mouse_position) {
			position = p_mouse_position;
		}


		public override List<RaycastResult> Hover(Vector3 p_mouse_position) {
			return GetRaycastResults(p_mouse_position);
		}

	}
}