﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace TrueAutomation {

public class EditorMouse : BaseMouse {
		bool isClick;

		public EditorMouse(EventSystem eventSystem) : base(eventSystem){
			
		}

		public void SetUp(Vector3 p_position, bool p_isClick) {
			position = p_position;
			isClick = p_isClick;
		}

		public void Execute(GameObject p_object, InputInfoComponent p_inputInfoComp) {
			ExecuteEvents.Execute(p_object, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
		}

		public override bool OnMouseDown(int p_event_id) {
			return isClick;
		}
		
		public override bool OnMouseUp(int p_event_id) {
			return isClick;
		}
		public override bool OnMouse(int p_event_id) {
			return isClick;
		}

		

	}
}