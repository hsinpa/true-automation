﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace TrueAutomation {

	public abstract class BaseMouse {
		protected Vector3 position;
		protected EventSystem eventSystem;

		public BaseMouse(EventSystem p_eventSystem) {
			eventSystem = p_eventSystem;
		}

		public abstract bool OnMouseDown(int p_event_id);
		public abstract bool OnMouseUp(int p_event_id);
		public abstract bool OnMouse(int p_event_id);



		public virtual void Tracking(Vector3 p_mouse_position) {

		}

		public virtual List<RaycastResult> Hover(Vector3 p_mouse_position) {
			return new List<RaycastResult>();
		}


		public virtual RaycastHit UIRayCast(int p_layerMask, Transform p_transform ) {

			RaycastHit hit;
			// Does the ray intersect any objects excluding the player layer
			if (Physics.Raycast(p_transform.position, p_transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, p_layerMask))
			{
				Debug.DrawRay(p_transform.position, p_transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
				Debug.Log("Did Hit");
			}

			return hit;
		}

		public virtual RaycastResult GetHitObject(Vector3 p_mouse_position) {
			List<RaycastResult> raycastResults =  GetRaycastResults(p_mouse_position);

			if (raycastResults.Count > 0) {
				return raycastResults[0];
			}
			
			return new RaycastResult();
		}

		protected List<RaycastResult> GetRaycastResults(Vector3 p_mouse_position) {
			//Create the PointerEventData with null for the EventSystem
			PointerEventData ped = new PointerEventData(null);
			//Set required parameters, in this case, mouse position
			ped.position = p_mouse_position;
			//Create list to receive all results
			List<RaycastResult> results = new List<RaycastResult>();
			//Raycast it
			eventSystem.RaycastAll(ped, results);

			return results;
		}

		


		
	}

}
