﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;
using System;

public class CreateTestCaseCSharp
{
    [MenuItem("TrueAutomation/MakeAutoTestCaseFile")]
    private static void SASFGetCustomUpdate()
    {
        MakeTestCaseCSharp();
    }

    private static string cSavePathBase = Path.Combine(Application.dataPath, "TrueAutomation/CSharp");
    private const string cDotCs = ".cs";
    private const string cWriteCSharpCaseName = "RecoredTestCases";
    private static List<InputInfoComponent> mWriteCases;
    private static string mLoadScene;

    public static void MakeTestCaseCSharp()
    {

        if (!Directory.Exists(cSavePathBase))
        {
            Debug.Log("Create:\n" + cSavePathBase);
            Directory.CreateDirectory(cSavePathBase);
        }
        MakeWriteCaseInfo();
        try
        {
            string aOutFilePath = Path.Combine(cSavePathBase, cWriteCSharpCaseName + cDotCs);
            FileStream aFS = new FileStream(aOutFilePath, FileMode.Create, FileAccess.Write);
            StreamWriter aSW = new StreamWriter(aFS, Encoding.GetEncoding("UTF-8"));
            // Write Data
            aSW.WriteLine("#if UNITY_EDITOR");
            aSW.WriteLine("using System.Collections;");
            aSW.WriteLine("using System.Collections.Generic;");
            aSW.WriteLine("using UnityEngine;");
            aSW.WriteLine("using UnityEngine.UI;");
            aSW.WriteLine("using TrueAutomation;");
            aSW.WriteLine("using UnityEngine.EventSystems;");
            aSW.WriteLine("using UnityEngine.SceneManagement;");
            aSW.WriteLine("using UnityEngine.TestTools;");
            aSW.WriteLine("using NUnit.Framework;\n");

            aSW.WriteLine("public class " + cWriteCSharpCaseName);
            aSW.WriteLine("{");
            aSW.WriteLine("\tEditorMouse editorMouse;");
            aSW.WriteLine("\t[UnityTest]");
            aSW.WriteLine("\tpublic IEnumerator ALoadScenes()");
            aSW.WriteLine("\t{");
            aSW.WriteLine("\t\tSceneManager.LoadScene(\"UISample/Scenes/"+ mLoadScene +"\");");
            aSW.WriteLine("\t\tyield return null;");
            aSW.WriteLine("\t\tyield return new WaitForSeconds(3f);");
            aSW.WriteLine("\t\tDebug.Log(\"Wait for 3 second.\");");
            aSW.WriteLine("\t\tif (GameObject.Find(\"Input\") != null)");
            aSW.WriteLine("\t\t{");
            aSW.WriteLine("\t\t\tGameObject.Find(\"Input\").GetComponent<RecorderManager>().recordModeActivate = false;");
            aSW.WriteLine("\t\t}");
            aSW.WriteLine("\t}");

            float aLastTime = 0;
            for (int i = 0; i < mWriteCases.Count; i++)
            {
                InputInfoComponent aTmp = mWriteCases[i];
                aSW.WriteLine("\t[UnityTest]");
                aSW.WriteLine("\tpublic IEnumerator RunTestCase"+ i.ToString("000") + "_" + aTmp.type +" ()");
                aSW.WriteLine("\t{");
                aSW.WriteLine("\t\teditorMouse = new EditorMouse(EventSystem.current);");
                aSW.WriteLine();
                aSW.WriteLine("\t\tyield return new WaitForSeconds(" + (aTmp.time - aLastTime) + "f);");
                aLastTime = aTmp.time;
                aSW.WriteLine("\t\tGameObject aTmpRayCast = editorMouse.GetHitObject(new Vector3(" + aTmp.worldPosition.x + "f, " + aTmp.worldPosition.y + "f, " + aTmp.worldPosition.z + "f)).gameObject;");
                aSW.WriteLine("\t\tstring aTmpType = \"" + aTmp.type + "\";");
                aSW.WriteLine("\t\tif (aTmpRayCast != null && aTmpType == \"MouseDown\")");
                aSW.WriteLine("\t\t{");
                aSW.WriteLine("\t\t\tDebug.Log(aTmpRayCast.name);");
                aSW.WriteLine("\t\t\tif (aTmpRayCast.GetComponentInParent<Button>() != null)");
                aSW.WriteLine("\t\t\t{");
                aSW.WriteLine("\t\t\t\taTmpRayCast.transform.GetComponentInParent<Button>().onClick.Invoke();");
                aSW.WriteLine("\t\t\t\tDebug.Log(\"ButtonClick.\");");
                aSW.WriteLine("\t\t\t}");
                aSW.WriteLine("\t\t\telse if (aTmpRayCast.GetComponentInParent<Toggle>() != null)");
                aSW.WriteLine("\t\t\t{");
                aSW.WriteLine("\t\t\t\taTmpRayCast.transform.GetComponentInParent<Toggle>().isOn = !aTmpRayCast.transform.GetComponentInParent<Toggle>().isOn;");
                aSW.WriteLine("\t\t\t\tDebug.Log(\"ToggleClick.\");");
                aSW.WriteLine("\t\t\t}");

                aSW.WriteLine();
                aSW.WriteLine("\t\t}");
                aSW.WriteLine();
                aSW.WriteLine("\t}");
            }


            aSW.WriteLine("}");
            aSW.WriteLine("#endif");
            // 結束寫入
            aSW.Close();
            aFS.Close();
            // Down
            Debug.Log("Write Done.");
            AssetDatabase.Refresh();
        }
        catch (IOException e)
        {
            Debug.Log(e.ToString());
        }
    }


    private static void MakeWriteCaseInfo()
    {
        StreamReader reader = new StreamReader("Assets/StreamingAssets/recording_files/recordJSONFile.json");
        string aJsonString = reader.ReadToEnd();
        mWriteCases = new List<InputInfoComponent>();
        try
        {
            JSONObject jsObject = new JSONObject(aJsonString);
            List<JSONObject> jsObjectList = jsObject.list;
            int behaviourCount = jsObject.list.Count;
            Debug.Log("count :" + behaviourCount);
            mLoadScene = jsObjectList[0].GetField("name").str;

            for (int i = 1; i < behaviourCount; i++)
            {
                InputInfoComponent aTmp = new InputInfoComponent();


                aTmp.type = jsObjectList[i].GetField("type").str;
                aTmp.time = jsObjectList[i].GetField("time").f;
                aTmp.worldPosition = new Vector3(jsObject[i].GetField("mousePosition").list[0].f, jsObject[i].GetField("mousePosition").list[1].f, 0f);
                aTmp.name = jsObject[i].GetField("name").str;
                aTmp.fullPath = jsObject[i].GetField("full_path").str;

                mWriteCases.Add(aTmp);
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }



}
#endif